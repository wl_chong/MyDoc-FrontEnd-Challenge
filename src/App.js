import React, {useState} from 'react';
import './index.css';

import Home from "./pages/Home";
import ViewSaved from "./pages/ViewSaved";

// Install react-router-dom to switch between pages
import { BrowserRouter, Route } from "react-router-dom";
import { CustomForm } from "./components/customForm";
import {LanguageContext, lang} from "./language-context";

function App() {
  const [currentLan, setLang] = useState(lang.en);
  console.log(currentLan)
  return (
    <div className="App">
    <CustomForm/>
      <LanguageContext.Provider value={{
        language:currentLan,
        toggleLang: () => setLang(currentLan.main === "en" ? lang.cn : lang.en)}}>
        <LanguageContext.Consumer>
          {({language, toggleLang}) => (
              <button
                  onClick={toggleLang}>
                {language.weather}
              </button>
          )}
        </LanguageContext.Consumer>
      </LanguageContext.Provider>
      <BrowserRouter>
        <Route exact path="/" component={Home}/>
        <Route path="/saved" component={ViewSaved}/>
      </BrowserRouter>
    </div>
  );
}

export default App;
