import React, { useMemo, useState, useEffect } from 'react';
const s = {a:123}
export const Count = React.memo(({onOdd}) => {
    const [count, setCount] = useState(0);
    console.log('render');
    return (
        <div>
            <div>{count}</div>
            <button onClick={() => {
                if (count % 2 === 0) {
                    onOdd()
                }

                setCount(count + 1)
            }}>+ Add</button>
        </div>
    )
})

export function CustomForm() {
    const [text, setText] = useState('');
    const onOdd = React.useCallback(() => setText(''), [setText]);
    const data = useMemo(() => ({ok: true}), [])

    const getData = async () => {
        console.log('init')
        const s = await fetch('https://jsonplaceholder.typicode.com/todos/1');
        const data = await s.json();
        console.log(data)
    }

    useEffect(() => {
        getData()
    }, [text])

    useEffect(() => {
        console.log('hi')
    }, [text])
    return (
        <div>
            <input placeholder="anything" value={text} onChange={(e) => setText(e.target.value)}/>
            <Count onOdd={onOdd} data={data} s={s}/>
        </div>
    )
}
