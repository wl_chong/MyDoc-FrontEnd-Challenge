import React from "react";

export const lang = {
    en: {
        main: "en",
        weather: 'I am a bad weather.',
        background: '#eeeeee',
    },
    cn: {
        main: "cn",
        weather: '天氣',
        background: '#222222',
    },
};

export const LanguageContext = React.createContext({
    lang: lang.en,
    toggleLang: () => {}
}
);
